/**
 * DBSyncer Copyright 2020-2023 All Rights Reserved.
 */
package org.test;

import org.dbsyncer.sdk.connector.ConnectorInstance;
import org.dbsyncer.sdk.connector.database.DatabaseConnectorInstance;
import org.dbsyncer.sdk.connector.database.ds.SimpleConnection;
import org.dbsyncer.sdk.plugin.PluginContext;
import org.dbsyncer.sdk.spi.PluginService;

public class MyPlugin implements PluginService {

    /**
     * 全量同步/增量同步
     *
     * @param pluginContext
     */
    @Override
    public void convert(PluginContext pluginContext) {
        // TODO 消费或处理数据
        System.out.println("插件消费数据中...");
        // 同步模式，全量/增量
        pluginContext.getModelEnum();

        // 是否终止同步到目标库开关，默认false
        pluginContext.setTerminated(false);

        // 数据源表和目标源表
        pluginContext.getSourceTableName();
        pluginContext.getTargetTableName();

        // 捕获的事件
        pluginContext.getEvent();

        // 数据源和目标源表全量或增量数据
        pluginContext.getSourceList();
        pluginContext.getTargetList();

        // 获取目标库连接器实例
        pluginContext.getTargetConnectorInstance();
    }

    /**
     * 全量同步/增量同步完成后执行处理
     *
     * @param context
     */
    @Override
    public void postProcessAfter(PluginContext context) {
        // TODO 完成同步后调用该方法
        ConnectorInstance connectorInstance = context.getSourceConnectorInstance();

        // 获取关系型数据库连接，实现自己的业务逻辑...
        if (connectorInstance instanceof DatabaseConnectorInstance) {
            DatabaseConnectorInstance db = (DatabaseConnectorInstance) connectorInstance;
            // 方式一（推荐）：
            String query = "select * from my_user";
            db.execute(databaseTemplate -> databaseTemplate.queryForList(query));

            // 方式二：
            SimpleConnection connection = null;
            try {
                // 通过JDBC访问数据库
                connection = (SimpleConnection) db.getConnection();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.close();
                }
            }
        }
    }

    /**
     * 重写方法：设置版本号
     *
     * @return
     */
    @Override
    public String getVersion() {
        return "2.0.5_0112";
    }

    /**
     * 重写方法：设置插件名称
     *
     * @return
     */
    @Override
    public String getName() {
        return "我的插件";
    }
}
