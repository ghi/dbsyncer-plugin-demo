#### 介绍
本项目提供开发者编写[dbsyncer](https://gitee.com/ghi/dbsyncer)（简称dbs）插件示例

> 插件有什么用？

插件是一种可扩展全量同步和增量同步实现数据转换的技术方式。通过插件可以接收同步数据，自定义同步到目标源的行数据，也能消费数据并实现更多业务场景。

## 📦开发说明
dbsyncer-plugin-demo是基于[dbsyncer](https://gitee.com/ghi/dbsyncer)项目做插件开发，所以需要依赖开发包

#### 配置依赖包：
> 通过maven命令，安装依赖包到本地仓库
``` mvn
# 下载dbs项目
git clone https://gitee.com/ghi/dbsyncer.git

cd dbsyncer-sdk

# 安装dbsyncer-sdk开发包到maven本地仓库中（首次安装或升级版本后需要）
mvn install -Dmaven.test.skip=true
```

> 当前项目pom.xml已引入依赖，如需升级或更换版本可以手动替换dbsyncer.version版本号
``` pom
<!-- 全局参数版本 -->
<properties>
    <dbsyncer.version>2.0.5</dbsyncer.version>
</properties>

<!-- 引入dbsyncer开发包 -->
<dependency>
    <groupId>org.ghi</groupId>
    <artifactId>dbsyncer-sdk</artifactId>
    <version>${dbsyncer.version}</version>
    <scope>compile</scope>
</dependency>
```

## ⚙️通过Maven命令打包
```
mvn clean compile package
```
![输入图片说明](https://foruda.gitee.com/images/1736667928756652965/7442d2db_376718.png "屏幕截图")
## 💕了解更多
* QQ群: 875519623或点击右侧按钮<a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=fce8d51b264130bac5890674e7db99f82f7f8af3f790d49fcf21eaafc8775f2a"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="数据同步dbsyncer" title="数据同步dbsyncer" /></a>